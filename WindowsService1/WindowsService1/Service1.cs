﻿using Nancy.Json;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;

namespace WindowsService1
{
    public partial class Service1 : ServiceBase
    {
        System.Timers.Timer timer = new System.Timers.Timer(); // name space(using System.Timers;)
        public Service1()
        {

            InitializeComponent();
        }
        protected override void OnStart(string[] args)
        {
            timer.Elapsed += new ElapsedEventHandler(OnElapsedTime);
            timer.Interval = 10000; //number in milisecinds  

            timer.Enabled = true;
        }
        protected override void OnStop()
        {
            // WriteToFile("Service is stopped at " + DateTime.Now);
        }
        private void OnElapsedTime(object source, ElapsedEventArgs e)
        {

            this.LogMessage("Service Running");
        }
        private void LogMessage(string Message)
        {
            SqlConnection connection = null;
            SqlCommand command = null;
            try
            {
                connection = new SqlConnection(@"Server=51.89.183.40,1533;Initial Catalog=MPC_Sport;Persist Security Info=False;User ID=user_mpcsport_live;Password=Mpcsport#111;MultipleActiveResultSets=False;Connection Timeout=30;");
                command = new SqlCommand("select m.marketType,m.MarketId,m.Result,s.BfSportID from Market m INNER JOIN Match ma ON ma.MatchID = m.MatchID INNER JOIN Tournament t ON t.TournamentID = ma.TournamentID INNER JOIN Sport s ON s.SportID = t.SportID where m.MarketStatus = 4 and m.IsSettled = 0 and m.Result <> '' and m.Result IS NOT NULL", connection);
                connection.Open();
                SqlDataAdapter adp = new SqlDataAdapter(command);
                DataTable dt = new DataTable();
                adp.Fill(dt);
                for (int i = 0; i < dt.Rows.Count; i++)
                {       
                    string marketType = dt.Rows[i]["marketType"].ToString(); // Change Field Name 
                    string bfSportId = dt.Rows[i]["BfSportID"].ToString(); // Change Field Name 
                    if (bfSportId == "7")
                    {
                        try
                        {
                            SqlCommand cmd = new SqlCommand("SettleHorseRacing", connection);
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Add("@MarketId", SqlDbType.VarChar).Value = dt.Rows[i]["MarketId"].ToString();
                            cmd.Parameters.Add("@Result", SqlDbType.VarChar).Value = dt.Rows[i]["Result"].ToString();
                            cmd.ExecuteNonQuery();
                        }
                        catch (Exception ex)
                        {
                            EventLog.WriteEntry("Èxception In SettleHorseRacing", "Message : " + ex.Message.ToString(), EventLogEntryType.Error);
                        }

                    }
                    else if (marketType == "LINE")
                    {
                        try
                        {
                            SqlCommand cmd = new SqlCommand("SettleLineMarket", connection);
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Add("@MarketId", SqlDbType.VarChar).Value = dt.Rows[i]["MarketId"].ToString();
                            cmd.Parameters.Add("@Result", SqlDbType.VarChar).Value = dt.Rows[i]["Result"].ToString();
                            cmd.ExecuteNonQuery();
                        }
                        catch (Exception ex)
                        {
                            EventLog.WriteEntry("Èxception In SettleLineMarket", "Message : " + ex.Message.ToString(), EventLogEntryType.Error);
                        }

                    }
                    else if (marketType == "AdvanceSession")
                    {
                        try
                        {
                            SqlCommand cmd = new SqlCommand("SettleSession", connection);
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Add("@MarketId", SqlDbType.VarChar).Value = dt.Rows[i]["MarketId"].ToString();
                            cmd.Parameters.Add("@Result", SqlDbType.VarChar).Value = dt.Rows[i]["Result"].ToString();
                            cmd.ExecuteNonQuery();
                        }
                        catch (Exception ex)
                        {
                            EventLog.WriteEntry("Èxception In SettleSession", "Message : " + ex.Message.ToString(), EventLogEntryType.Error);
                        }

                    }
                    else if (marketType == "Bookmakers")
                    {
                        try
                        {
                            SqlCommand cmd = new SqlCommand("SettleBookMakers", connection);
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Add("@MarketId", SqlDbType.VarChar).Value = dt.Rows[i]["MarketId"].ToString();
                            cmd.Parameters.Add("@Result", SqlDbType.VarChar).Value = dt.Rows[i]["Result"].ToString();
                            cmd.ExecuteNonQuery();
                        }
                        catch (Exception ex)
                        {
                            EventLog.WriteEntry("Èxception In SettleBookMakers", "Message : " + ex.Message.ToString(), EventLogEntryType.Error);
                        }

                    }
                    else
                    {
                        try
                        {
                            SqlCommand cmd = new SqlCommand("SettleMatchOdds", connection);
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Add("@MarketId", SqlDbType.VarChar).Value = dt.Rows[i]["MarketId"].ToString();
                            cmd.Parameters.Add("@Result", SqlDbType.VarChar).Value = dt.Rows[i]["Result"].ToString();
                            cmd.ExecuteNonQuery();
                        }
                        catch (Exception ex)
                        {
                            EventLog.WriteEntry("Èxception In SettleMatchOdds", "Message : " + ex.Message.ToString(), EventLogEntryType.Error);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Èxception In Settlement", "Message : " + ex.Message.ToString(), EventLogEntryType.Error);
            }
            finally
            {
                command.Dispose();
                connection.Dispose();
            }
        }
    }
}
